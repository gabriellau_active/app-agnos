import React, { ReactElement } from 'react'
import { useField } from 'formik'
import { Form } from 'react-bootstrap'

const Input = ({label, ...props}: any): ReactElement => {
  const [field, meta, helpers] = useField({
    name: props.name
  })

  const hasError = meta.touched && !!meta.error

  return (
    <Form.Group
      controlId={props.name}
    >
      {label && (
        <Form.Label>
          {label}
        </Form.Label>
      )}

      <Form.Control
        {...props}
        isInvalid={hasError}
        onChange={valor => {
          helpers.setTouched(true)
          helpers.setValue(valor)

          if (props.onChange) {
            props.onChange(valor)
          }
        }}
      />

      {hasError && (
        <Form.Control.Feedback type="invalid">
          {meta.error}
        </Form.Control.Feedback>
      )}
    </Form.Group>
  )
}

export default Input
