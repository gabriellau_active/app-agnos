import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { IModalResultados } from '../types';

function ModalResultados(props: IModalResultados ) {
  // avisa o componente pai de que precisa fechar o modal
  const onHide = () => {
    if (props.onHide) {
      props.onHide();
    }
  };

  const onClickSalvar = () => {
    // aqui você inclui a chamada para o backend que irá salvar os dados de fato

    if (props.onSave) {
      props.onSave();
    }
  };

  return (
    <>
      <Modal
        show={props.exibir}
        onHide={onHide}
      >
        <Modal.Header closeButton>
          <Modal.Title>Modal Resultados</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          <table className='table'>
            <thead>
              <tr>
                <th>Titulo</th>
                <th>Descrição</th>
              </tr>
            </thead>

            <tbody>
              {props.resultados.map((resultado, index) => (
                <tr key={index}>
                  <td>{resultado.titulo}</td>
                  <td>{resultado.descricao}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </Modal.Body>

        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={onHide}
          >
            Fechar
          </Button>

          <Button
            variant="primary"
            onClick={onClickSalvar}
          >
            Salvar
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default ModalResultados;
