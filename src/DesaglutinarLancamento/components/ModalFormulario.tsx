import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import ModalFormularioCampos from './ModalFormularioCampos';
import { IModalFormulario, IDadosForm, IResultado } from '../types';

function ModalFormulario(props: IModalFormulario ) {
  // avisa o componente pai de que precisa fechar o modal
  const onHide = () => {
    if (props.onHide) {
      props.onHide();
    }
  };

  const enviarDadosParaApiSimularOResultado = (values: IDadosForm) => {
    console.log(values)

    // Simulação de chamada para o backend
    const resultados: IResultado[] = [
      {
        titulo: "titulo 1",
        descricao: "descricao 1"
      },
      {
        titulo: "titulo 2",
        descricao: "descricao 2"
      }
    ];

    return resultados
  }

  const onSubmit = (values: IDadosForm) => {
    // aqui você irá enviar os dados para o backend antes de chamar o props.onSubmit
    const resultadoSimulacao = enviarDadosParaApiSimularOResultado(values)

    if (props.onSubmit) {
      props.onSubmit(resultadoSimulacao);
    }
  };

  const initialValues: IDadosForm = {
    firstName: '',
    lastName: '',
    email: '',
  }

  return (
    <>
      <Modal
        show={props.exibir}
        onHide={onHide}
      >
        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
        >
          <Form>
            <Modal.Header closeButton>
              <Modal.Title>Modal Formulário</Modal.Title>
            </Modal.Header>

            <Modal.Body>
              <ModalFormularioCampos />
            </Modal.Body>

            <Modal.Footer>
              <Button
                variant="secondary"
                onClick={onHide}
              >
                Fechar
              </Button>

              <Button
                type="submit"
                variant="primary"
              >
                Simular
              </Button>
            </Modal.Footer>
          </Form>
        </Formik>
      </Modal>
    </>
  );
}

export default ModalFormulario;
