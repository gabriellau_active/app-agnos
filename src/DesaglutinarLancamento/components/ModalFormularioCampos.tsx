import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Input from '../../components/Input';

// Este componente não precisa de props pois está integrado com o formik
// Caso o seu precise de mais personalização, pode adicionar props para passar algum dado
const ModalFormularioCampos = () => {
  return (
    <>
      <Row>
        <Col sm='12'>
          <Input name="firstName" label='First name' />
        </Col>

        <Col sm='12'>
          <Input name="lastName" label='Last name' />
        </Col>

        <Col sm='12'>
          <Input name="email" label='Email' />
        </Col>
      </Row>
    </>
  );
}

export default ModalFormularioCampos;
