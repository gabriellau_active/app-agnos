import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import ModalFormulario from './components/ModalFormulario';
import ModalResultados from './components/ModalResultados';
import { IResultado } from './types';

function DesaglutinarLancamento() {
  const [exibirModalFormulario, setExibirModalFormulario] = useState<boolean>(false);
  const [exibirModalResultados, setExibirModalResultados] = useState<boolean>(false);
  const [resultados, setResultados] = useState<IResultado[]>([]);

  const onSubmitFormulario = (resultadoSimulacao: IResultado[]) => {
    setExibirModalFormulario(false)

    setResultados(resultadoSimulacao)

    setExibirModalResultados(true)
  }

  const onSave = () => {
    // Esta é a última etapa
    // Após receber a confirmação que salvou os dados,
    // emita uma notificação e resete o estado do componente

    setExibirModalResultados(false)
    setResultados([])

    // Utilize o utilitário de Notificação do sistema
    alert("Dados salvos")
  }

  return (
    <>
      <Button onClick={() => setExibirModalFormulario(true)}>
        Desaglutinar
      </Button>

      <ModalFormulario
        exibir={exibirModalFormulario}
        onSubmit={onSubmitFormulario}
        onHide={() => setExibirModalFormulario(false)}
      />

      <ModalResultados
        exibir={exibirModalResultados}
        resultados={resultados}
        onHide={() => setExibirModalResultados(false)}
        onSave={() => onSave()}
      />
    </>
  );
}

export default DesaglutinarLancamento;
