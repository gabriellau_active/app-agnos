export interface IDadosForm {
  firstName: string;
  lastName: string;
  email: string;
}

export interface IResultado {
  titulo: string;
  descricao: string;
}

export interface IModalFormulario {
  exibir?: boolean;
  onHide?: Function;
  onSubmit?: (resultados: IResultado[]) => void;
}

export interface IModalResultados {
  exibir?: boolean;
  resultados: IResultado[];
  onHide?: Function;
  onSave?: Function;
}
