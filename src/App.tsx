import React from 'react';
import DesaglutinarLancamento from './DesaglutinarLancamento/DesaglutinarLancamento';

// O App é o componente do sistema que irá chamar o seu componente
function App() {
  return (
    <>
      <DesaglutinarLancamento />
    </>
  );
}

export default App;
